<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table = 'leads';
    protected $fillable = [
        'name','surname','email','number','message','salesRep','salesMail',
        'status','physical_add','invoice_add','description','budget','type',
        'size','quantity','colours','illuminated','delivery','origin'
    ];
}
