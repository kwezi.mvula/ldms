<?php

namespace App\Http\Controllers;

use App\User;
use App\Lead;
use App\Contact;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::count();

        $count = Contact::where('assigned',0)
        ->count();

        $leads = Contact::where('assigned',0)
        ->orderBy('created_at','desc')->take(5)->get();

        $countP = Lead::where('status','pending')
        ->count();

        $countA = Lead::where('status','accepted')
        ->count();

        $countD = Lead::where('status','declined')
        ->count();

        $totalB = Lead::sum('budget');

        $budget = 0;

        $countL = 0;

        $leadBud = Lead::get();

        $usersB = User::orderBy('name','asc')->Paginate(3);

        $salesRep = auth()->user()->name;
            
        $accepted = Lead::where('salesRep',$salesRep)
            ->where('status','accepted')
            ->count();

        $declined = Lead::where('salesRep',$salesRep)
            ->where('status','declined')
            ->count();

        $pending = Lead::where('salesRep',$salesRep)
            ->where('status','pending')
            ->count();

        $total = Lead::where('salesRep',$salesRep)
            ->count();

        $recent = Lead:: orderBy('created_at','asc')
            ->where('salesRep',$salesRep)
            ->take(5)
            ->get();

        // if(auth()->user()->role_id == 1){
            return view('pages.admin.dashboard',compact('totalB','leads','count','users','budget',
            'countA','usersB','countD','countP','countL','leadBud','total','pending','declined','accepted'))
            ->with('i', (request()->input('page', 1) - 1) * 3);
        // }else{ 
        //     return view('pages.sales.dashboard')->with([
        //         'accepted' => $accepted,
        //         'declined' => $declined,
        //         'pending' => $pending,
        //         'recent' => $recent,
        //         'total' => $total,
        //         ]); 
        // }
    }
}
