<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Lead;
use App\Contact;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function leads()
    {
        $salesRep = auth()->user()->name;

        $my_leads = Lead::orderBy('created_at','asc')
        ->where('salesRep' , $salesRep)
        ->where('status','accepted')
        ->orWhere('status','pending')
        ->simplePaginate(10);
        return view('pages.sales.my_leads',compact('my_leads'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function accepted()
    {
        $salesRep = auth()->user()->name;

        $accepted = Lead::inRandomOrder()
        ->where('salesRep' , $salesRep)
        ->where('status','accepted')
        ->simplePaginate(10);
        return view('pages.sales.acc_leads',compact('accepted'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function declined()
    {
        $salesRep = auth()->user()->name;

        $declined = Lead::inRandomOrder()
        ->where('salesRep' , $salesRep)
        ->where('status','declined')
        ->simplePaginate(10);
        return view('pages.sales.dec_leads',compact('declined'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function pending()
    {

        $salesRep = auth()->user()->name;

        $pending = Lead::inRandomOrder()
        ->where('salesRep' , $salesRep)
        ->where('status','pending')
        ->simplePaginate(10);
        return view('pages.sales.pen_leads',compact('pending'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function view($id){
        $view = Lead::find($id);
        return view('pages.sales.show',compact('view'));
    }

    public function viewP($id)
    {
        $viewP = Lead::find($id);
        return view('pages.sales.showP',compact('viewP'));  
    }

    public function viewA($id)
    {
        $viewA = Lead::find($id);
        return view('pages.sales.showA',compact('viewA'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
