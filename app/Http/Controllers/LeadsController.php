<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Lead;
use App\Contact;
use App\Mail\SendLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lead = new Lead;
        $username = $request->input('salesRep');
        $Mail = User::where('name',$username)
        ->value('email');
        $lead ->name=$request->input('name');
        $lead ->surname=$request->input('surname');
        $lead ->email=$request->input('email');
        $lead ->number=$request->input('number');
        $lead ->message=$request->input('message');
        $lead ->salesRep=$request->input('salesRep');
        $lead ->salesMail=$Mail;
        $lead ->status=$request->input('status');
        $lead ->physical_add=$request->input('physical_add');
        $lead ->invoice_add=$request->input('invoice_add');
        $lead ->description=$request->input('description');
        $lead ->budget=$request->input('budget');
        $lead ->type=$request->input('type');
        $lead ->size=$request->input('size');
        $lead ->quantity=$request->input('quantity');
        $lead ->colours=$request->input('colours');
        $lead ->illuminated=$request->input('illuminated');
        $lead ->delivery=$request->input('delivery');
        $lead ->origin=$request->input('origin');

        $id = $request->input('id');

        $assigned = Contact::where('id',$id)
        ->update(['assigned' => 1]);

        $lead->save();
        // Send email to SalesPerson
        // Mail::send(new SendLead($lead));

        
           return redirect()->action('AdminController@index')
           ->with('success','Lead assigned successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
     
        $status['status'] = $request->status;
        $leads = DB::table('leads')->where('id',$id)->update($status);
        return back();
        
    }

    public function del_assign(Request $request, $id)
    {
        $del['assigned'] = $request->assigned;
        $leads = DB::table('contacts')->where('id',$id)->update($del);
        return redirect()->action('AdminController@index')
           ->with('delete','Lead deleted successfully');
    }

    public function update(Request $request, $id)
    {
        $data = array();
        $data['physical_add'] = $request->physical_add;
        $data['invoice_add'] = $request->invoice_add;
        $data['description'] = $request->description;
        $data['budget'] = $request->budget;
        $data['type'] = $request->type;
        $data['size'] = $request->size;
        $data['quantity'] = $request->quantity;
        $data['colours'] = $request->colours;
        $data['illuminated'] = $request->illuminated;
        $data['delivery'] = $request->delivery;

        
            $lead = DB::table('leads')->where('id',$id)->update($data);
            return back();
                             
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lead = Lead::where('id',$id)->delete();
        return redirect()->action('AdminController@assignment')
        ->with('deleted','Lead deleted successfully');
    }
}
