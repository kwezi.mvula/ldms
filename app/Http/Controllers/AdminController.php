<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App\User;
use App\Lead;
use App\Contact;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Contact::where('assigned',0)
        ->orderBy('created_at','desc')->simplePaginate(10);
        
        return view('pages.admin.leads',compact('leads'))
               ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function assignment()
    {
        $assignments = Lead::orderBy('created_at','desc')->simplePaginate(10);
        return view('pages.admin.assignment',compact('assignments'))
               ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function users()
    {
        $users = User::orderBy('name','asc')->simplePaginate(10);
        return view('pages.admin.users',compact('users'))
               ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function report(Request $request){

        $method = $request->method();

        if($request->isMethod('post')){
            if($request->has('search')){
                $users = User::orderBy('name','asc')->get();

                $filters = Lead::where('salesRep', $request->salesRep)
                ->orWhereMonth('created_at', $request->month)
                ->orWhere('origin', $request->source)
                ->orWhere('status',$request->status)
                ->orWhere('type',$request->type)
                ->orderBy('created_at','asc')
                ->simplePaginate(10);

                $count = Lead::where('salesRep', $request->salesRep)
                ->orWhereMonth('created_at', $request->month)
                ->orWhere('origin', $request->source)
                ->orWhere('status',$request->status)
                ->orWhere('type',$request->type)
                ->count();

                return view('pages.admin.report')->with([
                    'count' => $count,
                    'users' => $users, 
                    'filters' => $filters,            
                    ]);
            }elseif($request->has('download')){
                $users = User::orderBy('name','asc')->get();

                $report = Lead::where('salesRep', $request->salesRep)
                ->orWhere('origin', $request->source)
                ->orWhere('status',$request->status)
                ->orWhere('type',$request->type)
                ->orderBy('created_at','asc')
                ->get();

                $count = Lead::where('salesRep', $request->salesRep)
                ->orWhere('origin', $request->source)
                ->orWhere('status',$request->status)
                ->orWhere('type',$request->type)
                ->count();

                
                $pdf = PDF::loadView('pdf_view', ['report' => $report,'count' => $count])->setPaper('a4', 'landscape');
                return $pdf->download('report.pdf');
            }
        }else{
            $users = User::orderBy('name','asc')->get();
            $filters = Lead::orderBy('salesRep','asc')->simplePaginate(10);
            $count = Lead::count();
            return view('pages.admin.report')->with([
                'count' => $count,
                'users' => $users, 
                'filters' => $filters,            
                ]);
        } 
    }


    public function passed(){
        $passed = Lead::orderBy('name','asc')
        ->where('status','declined')
        ->simplePaginate(10);

        return view('pages.admin.passed',compact('passed'))
        ->with('i', (request()->input('page', 1) - 1) * 10);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('name','asc')->get();
        return view('pages.admin.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lead = Contact::find($id);
        $salesReps = User::orderBy('name','asc')->get();
        return view('pages.admin.assign')->with([
            'lead' => $lead,
            'salesReps' => $salesReps,
            ]);
    }

    public function view($id){
        $view = Lead::find($id);
        return view('pages.admin.view',compact('view'));
    }

    public function info($id){
        $info = Contact::find($id);
        return view('pages.admin.info',compact('info'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lead = Lead::where('id',$id)->first();
        return view('pages.admin.edit',compact('lead'));
    }

    public function reassign($id)
    {
        $salesReps = User::orderBy('name','asc')->get();
        $lead = Lead::where('id',$id)->first();
        return view('pages.admin.reassign')->with([
            'lead' => $lead,
            'salesReps' => $salesReps,            
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $username = $request->input('salesRep');
        $Mail = User::where('name',$username)
        ->value('email');
        $reassign = array();
        $reassign['salesRep'] = $request->salesRep;
        $reassign['salesMail'] = $Mail;
        $reassign['status'] = $request->status;
        $lead = DB::table('leads')->where('id',$id)->update($reassign);

        return redirect()->action('AdminController@passed')
           ->with('success','Lead reassigned successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
