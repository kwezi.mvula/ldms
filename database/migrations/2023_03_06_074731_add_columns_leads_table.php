<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->string('description')->nullabel();
            $table->string('type')->nullabel();
            $table->string('size')->nullabel();
            $table->integer('quantity')->nullabel();
            $table->string('colours')->nullabel();
            $table->string('illuminated')->nullabel();
            $table->string('delivery')->nullabel();
            $table->string('origin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            //
        });
    }
}
