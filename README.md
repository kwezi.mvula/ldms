To run application make sure you remember to:

1. In your termminal go to root of project after repo and run (composer update) command to load vender files.
2. Copy .env.example file and rename it to .env, which is where you will edit database credentials.
3. After setting up database, run (php artisan migrate) command in your terminal to migrate all your tables to your created database.
4. To run project, run (php artisan serve) command in your terminal.
