<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
/*Admin Routes*/
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/users','AdminController@users');
    Route::get('/leads','AdminController@index');
    Route::get('/assign/lead/{id}','AdminController@show');
    Route::post('/save-lead','LeadsController@store')->name('lead.store');
    Route::get('/add-lead','AdminController@create');
    Route::get('/assignment','AdminController@assignment');
    Route::get('/edit/lead/{id}','AdminController@edit');
    Route::get('/view/lead/{id}','AdminController@view');
    Route::get('/view/info/{id}','AdminController@info');
    Route::post('/update/lead/{id}','LeadsController@update');
    Route::get('/delete/lead/{id}','LeadsController@destroy');
    Route::get('/passed','AdminController@passed');
    Route::get('/reassign/lead/{id}','AdminController@reassign');
    Route::post('/reassignment/lead/{id}','AdminController@update');
    Route::post('/report','AdminController@report');
    Route::get('/report','AdminController@report');
    Route::post('/delete/assign/{id}','LeadsController@del_assign');
/*Sales Routes*/
    Route::get('/my-leads','SalesController@leads');
    Route::get('/my-pending','SalesController@pending');
    Route::get('/my-accepted','SalesController@accepted');
    Route::get('/my-declined','SalesController@declined');
    Route::post('/update/status/{id}','LeadsController@status');
    Route::get('/view/lead/{id}','SalesController@view');
    Route::get('/view/pending/{id}','SalesController@viewP');
    Route::get('/view/accepted/{id}','SalesController@viewA');
    
});


