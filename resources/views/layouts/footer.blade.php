<footer class="footer">
    <div class="container-fluid">
        <div class="copyright">
            &copy; {{ now()->year }} {{ __('by') }}
            <a href="/dashboard" target="_blank">{{ __('Signs4sa.') }}</a> 
        </div>
    </div>
</footer>
