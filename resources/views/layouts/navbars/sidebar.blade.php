
<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ __('4sa') }}</a>
            <a href="#" class="simple-text logo-normal">{{ __('Dashboard') }}</a>
        </div>
        @if(auth()->user()->role_id == 1)
        <ul class="nav">
            <li>
                <a href="{{ asset('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li >
                <a href="{{ asset('users') }}">
                    <i class="tim-icons icon-single-02"></i>
                    <p>{{ __('Sales Team') }}</p>
                </a>
            </li>
            <li >
                <a href="{{ asset('leads') }}">
                    <i class="tim-icons icon-bullet-list-67"></i>
                    <p>{{ __('Assign Leads') }}</p>
                </a>
            </li>
            <li >
                <a href="{{ asset('assignment') }}">
                    <i class="tim-icons icon-badge"></i>
                    <p>{{ __('Assigned Leads ') }}</p>
                </a>
            </li>
            <li >
                <a href="{{ asset('passed') }}">
                    <i class="tim-icons icon-trash-simple"></i>
                    <p>{{ __('Passed Leads ') }}</p>
                </a>
            </li>
            <li >
                <a href="{{ asset('report') }}">
                    <i class="tim-icons icon-chart-bar-32"></i>
                    <p>{{ __('Leads Report ') }}</p>
                </a>
            </li>
        </ul>
        @else
        <ul class="nav">
            <li>
                <a href="{{ asset('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="nav-item dropdown right">
                <a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="tim-icons icon-badge"></i>
                    <p>{{ __('My Leads') }}</p>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ asset('my-leads') }}">Assigned</a>
                  <a class="dropdown-item" href="{{ asset('my-accepted') }}">Accepted</a>
                  <a class="dropdown-item" href="{{ asset('my-pending') }}">Pending</a>
                </div>
            </li>
        </ul>
        @endif
    </div>
</div>
