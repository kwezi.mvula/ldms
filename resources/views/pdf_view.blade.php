
<div class="row">
  <div class="col-md-12">
    <div class="pull-right">
    </div>
    <div class="card ">
      <div class="card-header">
        <div class="pull-left">
          <h4 class="card-title">Leads Report</h4>
        </div>
        <div class="pull-right">
            <h4 class="card-title">Number Of Leads:{{ $count }} </h4>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Assigned To
                </th>
                <th>
                  Status
                </th>
                <th>
                  Sign Type
                </th>
                <th>
                  Quantity
                </th>
                <th>Budget</th>
                <th>
                  Source
                </th>
                <th>
                  Date Assigned
                </th>
                <th>
                  Date Updated
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($report as $rep)
              <tr>
                <td>
                  {{ $rep->salesRep }}
                </td>
                <td>
                    {{ $rep->status }}
                </td>
                <td>
                  {{ $rep->type }}
                </td>
                <td>
                  {{ $rep->quantity }}
                </td>
                <td>
                  R {{ $rep->budget }}
                </td>
                <td>
                  {{ $rep->source }}
                </td>
                <td>
                  {{ $rep->created_at }}
                </td>
                <td>
                  {{ $rep->updated_at }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

