@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="pull-right">
    </div>
    <div class="card ">
      <div class="card-header">
        <div class="pull-left">
          <h4 class="card-title">Leads Report</h4>
        </div>
        <div class="pull-right">
            <h4 class="card-title">Number Of Leads: {{ $count }}</h4>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Assigned To
                </th>
                <th>
                  Status
                </th>
                <th>
                  Sign Type
                </th>
                <th>
                  Quantity
                </th>
                <th>Budget</th>
                <th>
                  Source
                </th>
                <th>
                  Date Assigned
                </th>
                <th>
                  Date Updated
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($filters as $filter)
              <tr>
                <td>
                  {{ $filter->salesRep }}
                </td>
                <td>
                    {{ $filter->status }}
                </td>
                <td>
                  {{ $filter->type }}
                </td>
                <td>
                  {{ $filter->quantity }}
                </td>
                <td>
                  R {{ $filter->budget }}
                </td>
                <td>
                  {{ $filter->origin }}
                </td>
                <td>
                  {{ $filter->created_at }}
                </td>
                <td>
                  {{ $filter->updated_at }}
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Leads Not Available
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>
          {!! $filters->links() !!}
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card ">
      <div class="card-header">
        <div class="pull-left">
          <h4 class="card-title">Search By</h4>
        </div>
      </div>
      <div class="card-body">
        <form action="{{ asset('report') }}" method="POST"  enctype="multipart/form-data">
          @csrf
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <div class="form-group">
                        <select class="custom-select" style="background-color: #bc202d;width:190px" name="salesRep" id="salesRep">
                            <option selected>Salesperson</option>
                            @foreach ( $users as $user )
                                <option value="{{ $user->name  }}">{{ $user->name }}</option>   
                            @endforeach      
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <div class="form-group">
                        <select class="custom-select" style="background-color: #bc202d;width:190px" name="type" id="type">
                            <option selected>Signage Type</option>
                            <option value="Banners&Flags">Banners&flags</option>
                            <option value="Cladding">Cladding</option>
                            <option value="Custom Sign">Custom Sign</option>
                            <option value="Cut-Out Letters">Cut-Out Letters</option>
                            <option value="Digital Prints">Digital Prints</option>
                            <option value="Digital Signage">Digital Signage</option>
                            <option value="Diplay Stands">Display Stands</option>
                            <option value="Fabricated Sign">Fabricated Signs</option>
                            <option value="LightBox">LightBox</option>
                            <option value="Neon Sign">Neon Sign</option>
                            <option value="Portable Display & Signs">Portable Display & Signs</option>
                            <option value="Pylon Sign">Pylon Sign</option>
                            <option value="Shop Fitting">Shop Fitting</option>
                            <option value="Vehicle Branding">Vehicle Branding</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                    <div class="form-group">
                        <select class="custom-select" style="background-color: #bc202d;width:190px" name="source" id="source">
                            <option selected>Source</option>
                            <option value="callin">CallIn</option>
                            <option value="walkin">WalkIn</option>
                            <option value="coldcall">ColdCall</option>
                            <option value="Signs4sa">Signs4sa</option>
                            <option value="Pylonsigns">PylonSigns</option>
                            <option value="Buildasign">BuildASign</option>
                            <option value="Neondesign">NeonDesign</option>
                            <option value="Signshopper">SignShopper</option>
                            <option value="Buildabanner">BuildABanner</option>
                            <option value="Neonconnection">NeonConnection</option>
                            <option value="Signagerentals">SignageRentals</option>
                            <option value="Signshoponline">SignsShopOnline</option>
                            <option value="Wrapadvertising">WrapAdvertising</option>
                            <option value="Safetysignsforsale">SafetySignsForSale</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="form-group">
                      <select class="custom-select" style="background-color: #bc202d;width:190px" name="status" id="status">
                          <option selected>Status</option>
                          <option value="accepted">Accepted</option>
                          <option value="declined">Passed</option>
                          <option value="pending">Pending</option>
                      </select>
                  </div>
              </div>
              <div class="col-xs-3 col-sm-3 col-md-3">
                <div class="form-group">
                  
                  <select class="custom-select" style="background-color: #bc202d;width:190px" name="month" id="month">
                    <option selected>Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                </div>
              </div>

                
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <button type="submit"  style="width:130px" class="btn btn-info btn-sm" name="search"><i class="tim-icons icon-zoom-split"></i></button>                        
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <button type="submit" class="btn btn-dark btn-sm" name="download">Download PDF</button>
                </div>    
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
