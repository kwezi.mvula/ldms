@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <div class="pull-left">
                <h4 class="card-title">Lead</h4>
            </div>
            <div class="pull-right">
                <a href="{{ asset('assignment') }}" class="btn btn-success mb-2">Back</a><br><br>
            </div>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <h4>Assigned To: {{ $view->salesRep }}</h4>
              <p>Status: {{ $view->status }}</p>
              <p>Date Assigned At: {{ $view->created_at }}</p>
              <p>Date Updated At: {{ $view->updated_at }}</p><br>
              <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Name: {{ $view->name }} {{ $view ->surname }}</p>
                  <p>Email: {{ $view->email }}</p>
                  <p>Number: {{ $view->number }}</p>
                  <p>Message: {{ $view->message }}</p>
                  <p>Physical Addrss: {{ $view->physical_add }}</p>            
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Invoice Addrss: {{ $view->invoice_add }}</p>
                  <p>Description: {{ $view->description }}</p>
                  <p>Budget: R{{ $view->budget }}</p>
                  <p>Signage Type: {{ $view->type }}</p>
                  <p>Is Sign Illuminated: {{ $view->illuminated }}<p>
                  <p>Size: {{ $view->size }}</p>
                  <p>Colours: {{ $view->colours }}</p>
                  <p>Delivery Method: {{ $view->delivery }}</p>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
