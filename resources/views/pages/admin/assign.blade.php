@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Lead</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <p>Name: {{ $lead->name }} {{ $lead ->surname }}</p>
              <p>Email: {{ $lead->email }}</p>
              <p>Number: {{ $lead->number }}</p>
              <p>Message: {{ $lead->message }}</p><br>
            </span>
          </div>
          <div class="alert alert-success">
            <h3>
              Edit Lead Information And Assign Lead:</h3>
              <form action="{{ route('lead.store') }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $lead->id }}" class="form-control" >
                <input type="hidden" name="name"  value="{{ $lead->name }}" class="form-control" > 
                <input type="hidden" name="surname"  value="{{ $lead ->surname }}" class="form-control" > 
                <input type="hidden" name="email"  value="{{ $lead->email }}" class="form-control" > 
                <input type="hidden" name="number"  value="{{ $lead->number }}" class="form-control" > 
                <input type="hidden" name="message"  value="{{ $lead->message  }}" class="form-control" > 
                <input type="hidden" name="status"  value="pending" class="form-control" > 
                <input type="hidden" name="origin"  value="{{ $lead->origin  }}" class="form-control" >
                <div class="row">
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Physical Adrress:</strong>
                      <textarea  name="physical_add"  style="height:150px" class="form-control tinymce-editor" value="{{ $lead->physical_add }}"></textarea>
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Invoice Adrress:</strong>
                      <textarea  name="invoice_add"  style="height:150px" class="form-control tinymce-editor" value="{{ $lead->invoice_add }}"></textarea>
                    </div>
                  </div>
                  <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="form-group">
                         <strong>Description:</strong>
                         <textarea  name="description"  style="height:150px" class="form-control tinymce-editor" value="{{ $lead->description }}"></textarea>
                    </div>
                 </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Budget:</strong>
                      <input type="number" name="budget" id="budget"  min="1" max="1000000"  class="form-control" value="{{ $lead->budget }}">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Signage Type:</strong>
                      <td>
                        <select class="custom-select" style="background-color: #233d62" name="type" id="type">
                          <option selected>{{ $lead->type }}</option>
                          <option value="Banners&Flags">Banners&flags</option>
                          <option value="Cladding">Cladding</option>
                          <option value="Custom Sign">Custom Sign</option>
                          <option value="Cut-Out Letters">Cut-Out Letters</option>
                          <option value="Digital Prints">Digital Prints</option>
                          <option value="Digital Signage">Digital Signage</option>
                          <option value="Diplay Stands">Display Stands</option>
                          <option value="Fabricated Sign">Fabricated Signs</option>
                          <option value="LightBox">LightBox</option>
                          <option value="Neon Sign">Neon Sign</option>
                          <option value="Portable Display & Signs">Portable Display & Signs</option>
                          <option value="Pylon Sign">Pylon Sign</option>
                          <option value="Shop Fitting">Shop Fitting</option>
                          <option value="Vehicle Branding">Vehicle Branding</option>
                        </select>
                      </td>
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Size:</strong>
                      <input type="text" name="size"   class="form-control" value="{{ $lead->size }}">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Quantity:</strong>
                      <input type="number" name="quantity" id="quantity"  min="1" max="1000000"  class="form-control" value="{{ $lead->quantity }}">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Colurs:</strong>
                      <input type="text" name="colours"   class="form-control" value="{{ $lead->colours }}">
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Is Sign Illuminated:</strong>
                      <td>
                        <select class="custom-select" style="background-color: #233d62" name="illuminated" id="illuminated">
                          <option selected>{{ $lead->illuminated }}</option>
                          <option value="Yes">Yes</option>
                          <option value="No">No</option>
                        </select>
                      </td>
                    </div>
                  </div>
                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                      <strong>Delivery Method:</strong>
                      <td>
                        <select class="custom-select" style="background-color: #233d62" name="delivery" id="delivery">
                          <option selected>{{ $lead->delivery }}</option>
                          <option value="Delivery">Delivery</option>
                          <option value="Collection">Collection</option>
                        </select>
                      </td>
                    </div>
                  </div>

                  <div class="col-xs-5 col-sm-5 col-md-5">
                    <strong>Assign To:</strong>
                    <div class="form-group">
                      <td>
                        <select class="custom-select" id="salesRep" style="background-color: #233d62" name="salesRep" required autocomplete="salesRep" autofocus>
                          <option selected>{{ __('Sales Rep') }}</option>
                          @forelse($salesReps as $salesRep)
                            <option  value="{{ $salesRep->name }}">{{ $salesRep->name }}</option>
                          @empty
                            <option>{{ __('Sales Rep') }}</option>
                          @endforelse     
                        </select>
                      </td>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

