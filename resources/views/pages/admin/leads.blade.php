@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <div class="pull-left">
          <h4 class="card-title"> Leads</h4>
        </div>
        <div class="pull-right">
          <a href="{{ url('add-lead') }}" class="btn btn-success mb-2">Add Lead</a><br><br>
        </div>
      </div>
      <div class="card-body">
        @if ($message = Session::get('success'))
						<div class="alert alert-success">
							<p>{{ $message }}</p>
						</div>
        @endif
        @if ($message = Session::get('delete'))
						<div class="alert alert-primary">
							<p>{{ $message }}</p>
						</div>
        @endif
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Surname
                </th>
                <th>
                  Email
                </th>
                <th>
                  Number
                </th>
                <th>
                  Message
                </th>
                <th>
                  Date
                </th>
                <th class="text-center">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($leads as $lead)
              <tr>
                <td>
                  {{ $lead->name }}
                </td>
                <td>
                  {{ $lead->surname }}
                </td>
                <td>
                  {{ $lead->email }}
                </td>
                <td>
                  {{ $lead->number }}
                </td>
                <td>
                  {{ $lead->message }}
                </td>
                <td>
                  {{ $lead->created_at }}
                </td>
                <td class="text-center">
                  <a href="{{ URL::to('assign/lead/'.$lead->id) }}" class="btn btn-info">Assign</a><br><br>
                  <a href="{{ URL::to('view/info/'.$lead->id) }}" style="width: 128px" class="btn btn-success" >View</a><br><br>
                  <form action="{{ url('delete/assign/'.$lead->id) }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="assigned"  value="3" class="form-control" > 
                      <button type="submit" class="btn btn-primary">Delete</button>
                  </form>
                </td>
               
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Leads Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $leads->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
