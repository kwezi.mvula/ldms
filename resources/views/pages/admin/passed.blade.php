@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
          <p>{{ $message }}</p>
        </div>
        @endif
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th >
                  Assigned To
                </th>
                <th >
                  Status
                </th>
                <th>
                  Date
                </th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($passed as $pass)
              <tr>
                <td>
                  {{ $pass->name }} 
                </td>
                <td>
                  {{ $pass->email }}
                </td>
                <td>
                  {{ $pass->number }}
                </td>
                <td>
                    {{ $pass->salesRep }}
                </td>
                <td>
                  {{ $pass->status }}
              </td>
                <td>
                  {{ $pass->created_at }}
                </td>
                <td>
                  <a href="{{ URL::to('reassign/lead/'.$pass->id) }}" class="btn btn-success">Reassign</a><br><br>
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Passed Leads Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $passed->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
