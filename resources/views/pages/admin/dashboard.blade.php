@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Number Of Users</h5>
                    <h3 class="card-title"><i class="tim-icons icon-single-02"></i>{{ $users }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <table class="table tablesorter " id="">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Number Of Leads
                                    </th>
                                    <th>
                                        Total Budget
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($usersB as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        @foreach($leadBud as $lead)
                                            @if(($lead->salesRep) == ($user->name))
                                                @php
                                                    $budget = $budget + $lead->budget;
                                                    $countL++
                                                @endphp 
                                            @endif
                                        @endforeach 
                                        <td>{{ $countL }}</td>
                                        <td>R {{ $budget }}</td> 
                                    </tr>   
                                @endforeach
                            </tbody>
                        </table>
                        {!! $usersB->links() !!}
                    </div>             
                </div>
            </div>
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Pipeline</h5>
                    <h3 class="card-title"><i class="tim-icons icon-bullet-list-67"></i>Total Budget: R{{ $totalB }}</h3>
                </div>
                <div class="card-body">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Total Number Of Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $count }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ URL::to('/leads') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Number Of Accepted Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $countA }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ URL::to('/assignment') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Number Of Declined Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $countD }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ URL::to('/passed') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center> 
                    </div>     
                </div>
            </div>
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Number Of Pending Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $countP }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">To change status on pending leads please click button below </h5><br>
                        <center><a href="{{ URL::to('/assignment') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center> 
                    </div>     
                </div>
            </div>
        </div>      
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Pending Leads (Requires Attention)</h5>
                    <h3 class="card-title"><i class="tim-icons icon-bell-55"></i>{{ $pending }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">To change status on pending leads please click button below </h5><br>
                        <center><a href="{{ asset('my-pending') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center> 
                    </div>     
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">My Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $total }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ asset('my-leads') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">My Accepted Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $accepted }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ asset('my-accepted') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">My Declined Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $declined }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ asset('my-declined') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center> 
                    </div>     
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card ">
                <div class="card-header">
                    <h4 class="card-title">Recent Leads</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table tablesorter" id="">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                      Name
                                    </th>
                                    <th>
                                      Surname
                                    </th>
                                    <th>
                                      Email
                                    </th>
                                    <th>
                                      Number
                                    </th>
                                    <th>
                                      Message
                                    </th>
                                    <th>
                                      Date
                                    </th>
                                  </tr>
                            </thead>
                            <tbody>
                                @forelse($leads as $lead)
                                <tr>
                                    <td>
                                    {{ $lead->name }}
                                    </td>
                                    <td>
                                    {{ $lead->surname }}
                                    </td>
                                    <td>
                                    {{ $lead->email }}
                                    </td>
                                    <td>
                                    {{ $lead->number }}
                                    </td>
                                    <td>
                                    {{ $lead->message }}
                                    </td>
                                    <td>
                                    {{ $lead->created_at }}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center">
                                    Leads Not Available
                                    </td>
                                </tr>
                                @endforelse  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


