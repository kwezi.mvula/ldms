@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <div class="pull-left">
                <h4 class="card-title">Lead</h4>
            </div>
            <div class="pull-right">
                <a href="{{ asset('leads') }}" class="btn btn-success mb-2">Back</a><br><br>
            </div>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <h4>Name: {{ $info->name }} {{ $info->surname }}</h4><br>
              <p>Email: {{ $info->email }}</p>
              <p>Message: {{ $info->message }}</p><br>
              <p>Date: {{ $info->created_at }}</p>
            </span>
          </div>
          <a href="{{ URL::to('assign/lead/'.$info->id) }}" class="btn btn-info">Assign</a>
          <form action="{{ url('delete/assign/'.$info->id) }}" method="POST"  enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="assigned"  value="3" class="form-control" > 
              <button type="submit" class="btn btn-primary">Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection
