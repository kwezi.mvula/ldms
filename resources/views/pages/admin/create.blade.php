@extends('layouts.app')

@section('content')

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Lead</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-success">
            <h3>
              Add Lead:</h3>
              <form action="{{ route('lead.store') }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Surname:</strong>
                            <input type="text" name="surname" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Phone Number:</strong>
                            <input type="number" name="number" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Email:</strong>
                            <input type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Physical Adrress:</strong>
                            <textarea  name="physical_add"  style="height:150px" class="form-control tinymce-editor"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Invoice Adrress:</strong>
                            <textarea  name="invoice_add"  style="height:150px" class="form-control tinymce-editor"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10">
                        <div class="form-group">
                            <strong>Description:</strong>
                            <textarea  name="description"  style="height:150px" class="form-control tinymce-editor"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Budget:</strong>
                            <input type="number" name="budget" id="budget"  min="1" max="1000000"  class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Signage Type:</strong>
                            <select class="custom-select" style="background-color: #233d62" name="type" id="type">
                                <option selected>Select Signage Type</option>
                                <option value="Banners&Flags">Banners&flags</option>
                                <option value="Cladding">Cladding</option>
                                <option value="Custom Sign">Custom Sign</option>
                                <option value="Cut-Out Letters">Cut-Out Letters</option>
                                <option value="Digital Prints">Digital Prints</option>
                                <option value="Digital Signage">Digital Signage</option>
                                <option value="Diplay Stands">Display Stands</option>
                                <option value="Fabricated Sign">Fabricated Signs</option>
                                <option value="LightBox">LightBox</option>
                                <option value="Neon Sign">Neon Sign</option>
                                <option value="Portable Display & Signs">Portable Display & Signs</option>
                                <option value="Pylon Sign">Pylon Sign</option>
                                <option value="Shop Fitting">Shop Fitting</option>
                                <option value="Vehicle Branding">Vehicle Branding</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Size:</strong>
                            <input type="text" name="size"   class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Quantity:</strong>
                            <input type="number" name="quantity" id="quantity"  min="1" max="1000000"  class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Colurs:</strong>
                            <input type="text" name="colours"   class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Is Sign Illuminated:</strong>
                            <select class="custom-select" style="background-color: #233d62" name="illuminated" id="illuminated">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Delivery Method:</strong>
                            <select class="custom-select" style="background-color: #233d62" name="delivery" id="delivery">
                                <option selected>Select Delivery Method</option>
                                <option value="Delivery">Delivery</option>
                                <option value="Collection">Collection</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Lead Source:</strong>
                            <select class="custom-select" style="background-color: #233d62" name="origin" id="origin">
                                <option selected>Select Source</option>
                                <option value="callin">CallIn</option>
                                <option value="walkin">WalkIn</option>
                                <option value="coldcall">ColdCall</option>
                                <option value="Signs4sa">Signs4sa</option>
                                <option value="Pylonsigns">PylonSigns</option>
                                <option value="Buildasign">BuildASign</option>
                                <option value="Neondesign">NeonDesign</option>
                                <option value="Signshopper">SignShopper</option>
                                <option value="Buildabanner">BuildABanner</option>
                                <option value="Neonconnection">NeonConnection</option>
                                <option value="Signagerentals">SignageRentals</option>
                                <option value="Signshoponline">SignsShopOnline</option>
                                <option value="Wrapadvertising">WrapAdvertising</option>
                                <option value="Safetysignsforsale">SafetySignsForSale</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                            <strong>Assigned To:</strong>
                            <select class="custom-select" style="background-color: #233d62" name="salesRep" id="salesRep">
                                <option selected>Select Salesperson</option>
                                @foreach ( $users as $user )
                                    <option value="{{ $user->name  }}">{{ $user->name }}</option>   
                                @endforeach      
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="status"  value="pending" class="form-control" >
                    <input type="hidden" name="message"  value="None" class="form-control" >  

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
