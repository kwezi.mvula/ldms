@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        @if ($message = Session::get('deleted'))
					<div class="alert alert-danger">
						<p>{{ $message }}</p>
					</div>
				@endif
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th >
                  Assigned To
                </th>
                <th >
                  Status
                </th>
                <th>
                  Date
                </th>
                <th>
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($assignments as $assign)
              <tr>
                <td>
                  {{ $assign->name }} 
                </td>
                <td>
                  {{ $assign->email }}
                </td>
                <td>
                  {{ $assign->number }}
                </td>
                <td>
                    {{ $assign->salesRep }}
                </td>
                <td>
                  {{ $assign->status }}
              </td>
                <td>
                  {{ $assign->created_at }}
                </td>
                <td>
                  <a href="{{ URL::to('edit/lead/'.$assign->id) }}" style="width:150px" class="btn btn-success">Edit</a><br><br>
                  <a href="{{ URL::to('view/lead/'.$assign->id) }}" style="width:150px" class="btn btn-info">Read More</a><br><br>
                  <a href="{{ URL::to('delete/lead/'.$assign->id) }}" style="width:150px" class="btn btn-primary" onclick="return confirm('Are you sure you?')">Delete</a>
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Assignment Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $assignments->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
