@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Sales Team</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Email
                </th>   
              </tr>
            </thead>
            <tbody>
              @forelse($users as $user)
              <tr>
                <td>
                  {{ $user->name }} 
                </td>
                <td>
                  {{ $user->email }}
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  No Sales Reps
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $users->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
