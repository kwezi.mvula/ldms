@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Lead</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <h4>Assigned To: {{ $lead->salesRep }}</h4>
              <p>Status: {{ $lead->status }}</p>
              <p>Date Assigned At: {{ $lead->created_at }}</p>
              <p>Date Updated At: {{ $lead->updated_at }}</p><br>
              <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Name: {{ $lead->name }} {{ $lead ->surname }}</p>
                  <p>Email: {{ $lead->email }}</p>
                  <p>Number: {{ $lead->number }}</p>
                  <p>Message: {{ $lead->message }}</p>
                  <p>Physical Addrss: {{ $lead->physical_add }}</p>            
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Invoice Addrss: {{ $lead->invoice_add }}</p>
                  <p>Description: {{ $lead->description }}</p>
                  <p>Budget: R{{ $lead->budget }}</p>
                  <p>Signage Type: {{ $lead->type }}</p>
                  <p>Is Sign Illuminated: {{ $lead->illuminated }}<p>
                  <p>Size: {{ $lead->size }}</p>
                  <p>Colours: {{ $lead->colours }}</p>
                  <p>Delivery Method: {{ $lead->delivery }}</p>
                </div>
              </div>
            </span>
          </div>
          <div class="alert alert-success">
            <h3>
              Reassign Lead:</h3>
              <form action="{{ url('reassignment/lead/'.$lead->id) }}" method="POST"  enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <input type="hidden" name="status"  value="pending" class="form-control" > 
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="form-group">
                          <select class="custom-select" id="salesRep" style="background-color: #233d62" name="salesRep" required autocomplete="salesRep" autofocus>
                            <option>{{ __('Sales Rep') }}</option>
                            @forelse($salesReps as $salesRep)
                              <option  value="{{ $salesRep->name }}">{{ $salesRep->name }}</option>
                            @empty
                              <option>{{ __('Sales Rep') }}</option>
                            @endforelse     
                          </select>
                        </div>
                      </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <button type="submit" class="btn btn-primary">Reassign</button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection

