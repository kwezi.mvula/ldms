@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th>
                  Status
                </th>
                <th >
                  Date
                </th>
                <th>
                  Updated At
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($declined as $dec)
              <tr>
                <td>
                  {{ $dec->name }} 
                </td>
                <td>
                  {{ $dec->email }}
                </td>
                <td>
                  {{ $dec->number }}
                </td>
                <td>
                    {{ $dec->status }}
                </td>
                <td>
                    {{ $dec->created_at }}  
                </td>
                <td>
                    {{ $dec->updated_at }}
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Declined Leads Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $declined->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
