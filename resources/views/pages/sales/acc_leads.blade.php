@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th>
                  Status
                </th>
                <th>
                  Action
                </th>
                <th >
                  Date
                </th>
                <th>
                  Updated At
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($accepted as $acc)
              <tr>
                <td>
                  {{ $acc->name }} 
                </td>
                <td>
                  {{ $acc->email }}
                </td>
                <td>
                  {{ $acc->number }}
                </td>
                <td>
                    {{ $acc->status }}
                </td>
                <td>
                  <a href="{{ URL::to('edit/lead/'.$acc->id) }}" style="width:150px" class="btn btn-success">Edit</a><br><br>
                  <a href="{{ URL::to('view/accepted/'.$acc->id) }}" style="width:150px" class="btn btn-primary">Read More</a>
                </td>
                <td>
                    {{ $acc->created_at }}  
                </td>
                <td>
                    {{ $acc->updated_at }}
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Accepted Leads Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $accepted->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
