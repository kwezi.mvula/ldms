@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">Pending Leads (Requires Attention)</h5>
                    <h3 class="card-title"><i class="tim-icons icon-bell-55"></i>{{ $pending }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">To change status on pending leads please click button below </h5><br>
                        <center><a href="{{ asset('my-pending') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center> 
                    </div>     
                </div>
            </div>
        </div>
        
        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category"> Accepted Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $accepted }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ asset('my-accepted') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4">
            <div class="card card-chart">
                <div class="card-header">
                    <h5 class="card-category">My Leads</h5>
                    <h3 class="card-title"><i class="tim-icons icon-notes"></i>{{ $total }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-header">
                        <h5 class="card-category">For more information about leads please click button below </h5><br>
                        <center><a href="{{ asset('my-leads') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></center>
                    </div>      
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card ">
                <div class="card-header">
                    <h4 class="card-title">Recent Leads</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table tablesorter" id="">
                            <thead class=" text-primary">
                                <tr>
                                    <th>
                                      Name
                                    </th>
                                    <th>
                                      Surname
                                    </th>
                                    <th>
                                      Email
                                    </th>
                                    <th>
                                      Number
                                    </th>
                                    <th>
                                      Message
                                    </th>
                                    <th>
                                      Date
                                    </th>
                                  </tr>
                            </thead>
                            <tbody>
                                @forelse($recent as $rec)
                                <tr>
                                    <td>
                                    {{ $rec->name }}
                                    </td>
                                    <td>
                                    {{ $rec->surname }}
                                    </td>
                                    <td>
                                    {{ $rec->email }}
                                    </td>
                                    <td>
                                    {{ $rec->number }}
                                    </td>
                                    <td>
                                    {{ $rec->message }}
                                    </td>
                                    <td>
                                    {{ $rec->created_at }}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td class="text-center">
                                    Leads Not Available
                                    </td>
                                </tr>
                                @endforelse  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


