
@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <div class="pull-left">
                <h4 class="card-title">Lead</h4>
            </div>
            <div class="pull-right">
                <a href="{{ asset('home') }}" class="btn btn-success mb-2">Back</a><br><br>
            </div>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <h4>Assigned To: {{ $viewA->salesRep }}</h4>
              <p>Status: {{ $viewA->status }}</p>
              <p>Date Assigned At: {{ $viewA->created_at }}</p>
              <p>Date Updated At: {{ $viewA->updated_at }}</p><br>
              <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Name: {{ $viewA->name }} {{ $viewA ->surname }}</p>
                  <p>Email: {{ $viewA->email }}</p>
                  <p>Number: {{ $viewA->number }}</p>
                  <p>Message: {{ $viewA->message }}</p>
                  <p>Physical Addrss: {{ $viewA->physical_add }}</p>            
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Invoice Addrss: {{ $viewA->invoice_add }}</p>
                  <p>Description: {{ $viewA->description }}</p>
                  <p>Budget: R{{ $viewA->budget }}</p>
                  <p>Signage Type: {{ $viewA->type }}</p>
                  <p>Is Sign Illuminated: {{ $viewA->illuminated }}<p>
                  <p>Size: {{ $viewA->size }}</p>
                  <p>Colours: {{ $viewA->colours }}</p>
                  <p>Delivery Method: {{ $viewA->delivery }}</p>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
