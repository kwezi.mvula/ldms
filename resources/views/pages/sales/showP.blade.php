
@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <div class="pull-left">
                <h4 class="card-title">Lead</h4>
            </div>
            <div class="pull-right">
                <a href="{{ asset('home') }}" class="btn btn-success mb-2">Back</a><br><br>
            </div>
        </div>
        <div class="card-body">
          <div class="alert alert-info alert-with-icon" data-notify="container">
            <span data-notify="icon" class="tim-icons icon-bell-55"></span>
            <span data-notify="message">
              <h4>Assigned To: {{ $viewP->salesRep }}</h4>
              <p>Status: {{ $viewP->status }}</p>
              <p>Date Assigned At: {{ $viewP->created_at }}</p>
              <p>Date Updated At: {{ $viewP->updated_at }}</p><br>
              <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Name: {{ $viewP->name }} {{ $viewP ->surname }}</p>
                  <p>Email: {{ $viewP->email }}</p>
                  <p>Number: {{ $viewP->number }}</p>
                  <p>Message: {{ $viewP->message }}</p>
                  <p>Physical Addrss: {{ $viewP->physical_add }}</p>            
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5">
                  <p>Invoice Addrss: {{ $viewP->invoice_add }}</p>
                  <p>Description: {{ $viewP->description }}</p>
                  <p>Budget: R{{ $viewP->budget }}</p>
                  <p>Signage Type: {{ $viewP->type }}</p>
                  <p>Is Sign Illuminated: {{ $viewP->illuminated }}<p>
                  <p>Size: {{ $viewP->size }}</p>
                  <p>Colours: {{ $viewP->colours }}</p>
                  <p>Delivery Method: {{ $viewP->delivery }}</p>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
