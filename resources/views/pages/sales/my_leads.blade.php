@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th>
                  Status
                </th>
                <th>
                  Action
                </th>
                <th>
                  Date
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($my_leads as $assign)
              <tr>
                <td>
                  {{ $assign->name }} 
                </td>
                <td>
                  {{ $assign->email }}
                </td>
                <td>
                  {{ $assign->number }}
                </td>
                @if($assign->status=='pending')
                  <form action="{{ url('update/status/'.$assign->id) }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="status"  value="accepted" class="form-control" > 
                    <td>
                      <button type="submit" style="width:150px" class="btn btn-success btn-sm">Accept</button><br>
                  </form>
                  <form action="{{ url('update/status/'.$assign->id) }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="status"  value="declined" class="form-control" > 
                      <button type="submit" style="width:150px" class="btn btn-danger btn-sm">Pass</button>
                    </td>
                  </form>
                  <td>
                    <a href="{{ URL::to('view/lead/'.$assign->id) }}" class="btn btn-primary">Read More</a>
                  </td>
                  <td>
                    {{ $assign->created_at }}
                  </td>
                @else
                <td>
                  {{ $assign->status }}
                </td>
                <td>
                  <a href="{{ URL::to('view/lead/'.$assign->id) }}" class="btn btn-primary">Read More</a>
                </td>
                <td>
                  {{ $assign->created_at }}
                </td>
                @endif
                
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Assignment Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $my_leads->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
