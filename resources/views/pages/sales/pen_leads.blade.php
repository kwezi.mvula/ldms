@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header">
        <h4 class="card-title"> Lead Assignments</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table tablesorter " id="">
            <thead class=" text-primary">
              <tr>
                <th>
                  Client Name
                </th>
                <th>
                  Client Email
                </th>
                <th>
                  Client Number
                </th>
                <th>
                  Status
                </th>
                <th colspan="2">
                  Action
                </th>
                <th>
                  Date
                </th>
              </tr>
            </thead>
            <tbody>
              @forelse($pending as $pen)
              <tr>
                <td>
                  {{ $pen->name }} 
                </td>
                <td>
                  {{ $pen->email }}
                </td>
                <td>
                  {{ $pen->number }}
                </td>
                <form action="{{ url('update/status/'.$pen->id) }}" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="status"  value="accepted" class="form-control" > 
                    <td>
                      <button type="submit" style="width:150px" class="btn btn-success btn-sm">Accept</button>   
                </form>
                <form action="{{ url('update/status/'.$pen->id) }}" method="POST"  enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="status"  value="declined" class="form-control" > 
                      <button type="submit" style="width:150px" class="btn btn-danger btn-sm">Pass</button>
                    </td>
                </form>
                <td>
                  <a href="{{ URL::to('view/pending/'.$pen->id) }}" class="btn btn-primary">Read More</a>
                </td>
                <td>
                  {{ $pen->created_at }}
                </td>
              </tr>
              @empty
              <tr>
                <td class="text-center">
                  Pending Leads Not Available
                </td>
              </tr>
              @endforelse  
            </tbody>
          </table>
          {!! $pending->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
